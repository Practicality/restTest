<?php

define('CLASS_DIR',  __DIR__.'/class/');	
set_include_path(get_include_path().PATH_SEPARATOR.CLASS_DIR);
spl_autoload_register(); // default autoloader.

require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/bootstrap.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;

error_reporting(E_ALL);
ini_set("display_errors", 1);

$app = new Silex\Application();
$app['debug'] = true;

$app->before(function() use ($app)
{
    if (!isset($_SERVER['PHP_AUTH_USER']))
    {
        header('WWW-Authenticate: Basic realm="Please authenticate"');
        return $app->json(array('message' => 'Not Authorised', 'status' => 'failure'), 401);
    }
    else
    {
        $users = array(
            'jeff' => 'test'
            );

		if (!isset($users[$_SERVER['PHP_AUTH_USER']])) {
			return $app->json(array('message' => 'Unknown User', 'status' => 'failure'), 403);
		}

        if($users[$_SERVER['PHP_AUTH_USER']] !== $_SERVER['PHP_AUTH_PW'])
        {
            return $app->json(array('message' => 'Incorrect Password', 'status' => 'failure'), 403);
        }

    }
});

$app->before(function (Request $request) { // read json data
	if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
		$data = json_decode($request->getContent(), true);
		$request->request->replace(is_array($data) ? $data : array());
	}
});


//configure database connection
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
	'db.options' => array(
				'driver' => 'pdo_mysql',
				'host' => '127.0.0.1',
				'dbname' => 'rest',
				'user' => 'root',
				'password' => '',
				'charset' => 'utf8',
				),
			));

$app->get('/', function() use ($app) {
		// get all apis

		$resources = array(
			array("name" => "category", "url" => "/category"),
			array("name" => "gallery", "url" => "/gallery"),
			array("name" => "country", "url" => "/country"),
			array("name" => "artist", "url" => "/artist"),
			array("name" => "artwork", "url" => "/artwork"),

		);
		
		return $app->json(array('status' => 'success', 'result' => $resources, 'X-Total-Count' => count($resources)), 200);

	});

$app->get('/category', function() use ($app) {
		// get all categories

		$sql = "SELECT * FROM category";
		$categories = $app['db']->fetchAll($sql);
		
		return $app->json(array('status' => 'success', 'result' => $categories, 'X-Total-Count' => count($categories)), 200);

	});

$app->get('/category/{id}', function ($id) use($app) {
	// get 1 category
	$sql = "SELECT * FROM category WHERE id_category = ?";
	$post = $app['db']->fetchAssoc($sql, array((int) $id));
		
	if (!$post) {
		return $app->json(array('message' => 'Category not found', 'status' => 'failure'), 404);
	}

	return $app->json(array('status' => 'success', 'result' => $post, 'X-Total-Count' => 1), 200);

});

$app->post('/category', function (Request $request) use($entityManager, $app) {
	// create new category
	$category = new category();
	if (!$label = $request->get('label')) {
		return $app->json(array('message' => "Field label is required for category", 'status' => 'failure'), 400);
	}
	if (!$seoName = $request->get('seoName')) {
		return $app->json(array('message' => "Field seoName is required for category", 'status' => 'failure'), 400);
	}
	$category->setLabel($label);
	$category->setSeoName($seoName);
	$entityManager->persist($category);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Category " . $category->getIdCategory() . " created"), 201);

});

$app->put('/category/{id}', function (Request $request, $id) use ($entityManager, $app) {
	// update category

	$category = $entityManager->find('category', $id);

	if (!$category) {
		return $app->json(array('message' => "Category not found", 'status' => 'failure'), 404);
	}
	if (!$label = $request->get('label')) {
		return $app->json(array('message' => "Field label is required for category", 'status' => 'failure'), 400);
	}
	if (!$seoName = $request->get('seoName')) {
		return $app->json(array('message' => "Field seoName is required for category", 'status' => 'failure'), 400);
	}
	$category->setLabel($label);
	$category->setSeoName($seoName);
	$entityManager->persist($category);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Category $id updated"), 200);
});

$app->delete("/category/{id}", function($id) use($entityManager, $app) {
	$category = $entityManager->find('category', $id);

	if (!$category) {
		return $app->json(array('message' => "Category not found", 'status' => 'failure'), 404);
	}

	$entityManager->remove($category);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Category $id deleted"), 200);
});


$app->get('/gallery', function() use ($app) {
	// get all galleries

	$sql = "SELECT * FROM gallery";
	$galleries = $app['db']->fetchAll($sql);
		
	return $app->json(array('status' => 'success', 'result' => $galleries, 'X-Total-Count' => count($galleries)), 200);

});

$app->get('/gallery/{id}', function ($id) use($app) {
	// get 1 category
	$sql = "SELECT * FROM gallery WHERE id_gallery = ?";
	$post = $app['db']->fetchAssoc($sql, array((int) $id));
		
	if (!$post) {
		return $app->json(array('message' => 'Gallery not found', 'status' => 'failure'), 404);
	}
	return $app->json(array('status' => 'success', 'result' => $post, 'X-Total-Count' => 1), 200);
});

$app->post('/gallery', function (Request $request) use($entityManager, $app) {
	// create new gallery
	$gallery = new gallery();
	if (!$name = $request->get('name')) {
		return $app->json(array('message' => "Field name is required for gallery", 'status' => 'failure'), 400);
	}
	if (!$email = $request->get('email')) {
		return $app->json(array('message' => "Field email is required for gallery", 'status' => 'failure'), 400);
	}
	$gallery->setName($name);
	$gallery->setEmail($email);
	$entityManager->persist($gallery);
	$entityManager->flush();
	return $app->json(array('status' => 'success', 'result' => "Gallery " . $gallery->getIdGallery() . " created"), 201);
	
});

$app->put('/gallery/{id}', function (Request $request, $id) use ($entityManager, $app) {
	// update category

	$gallery = $entityManager->find('gallery', $id);

	if (!$gallery) {
		return $app->json(array('message' => "Gallery not found", 'status' => 'failure'), 404);
	}
	if (!$name = $request->get('name')) {
		return new Response("Field name is required for gallery", 400);
	}
	if (!$email = $request->get('email')) {
		return new Response("Field email is required for gallery", 400);
	}
	$gallery->setName($name);
	$gallery->setEmail($email);
	$entityManager->persist($gallery);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Gallery $id updated"), 200);
});

$app->delete("/gallery/{id}", function($id) use($entityManager, $app) {
	$gallery = $entityManager->find('gallery', $id);

	if (!$gallery) {
		return $app->json(array('message' => "Gallery not found", 'status' => 'failure'), 404);
	}

	$entityManager->remove($gallery);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Gallery $id deleted"), 200);
});

$app->get('/country', function() use ($app) {
	// get all countries

	$sql = "SELECT * FROM country";
	$countries = $app['db']->fetchAll($sql);
	
	return $app->json(array('status' => 'success', 'result' => $countries, 'X-Total-Count' => count($countries)), 200);	

});

$app->get('/country/{id}', function ($id) use($app) {
	// get 1 country
	$sql = "SELECT * FROM country WHERE id_country = ?";
	$post = $app['db']->fetchAssoc($sql, array((int) $id));
		
	if (!$post) {
		return $app->json(array('message' => 'Country not found', 'status' => 'failure'), 404);
	}

	return $app->json(array('status' => 'success', 'result' => $post, 'X-Total-Count' => 1), 200);

});

$app->post('/country', function (Request $request) use($entityManager, $app) {
	// create new country
	$country = new Country();
	if (!$label_fr = $request->get('label_fr')) {
		return $app->json(array('message' => "Field lable_fr is required for country", 'status' => 'failure'), 400);
	}
	if (!$label_en = $request->get('label_en')) {
		return $app->json(array('message' => "Field label_en is required for country", 'status' => 'failure'), 400);
	}
	if (!$seo_url_fr = $request->get('seo_url_fr')) {
		return $app->json(array('message' => "Field seo_url_fr is required for country", 'status' => 'failure'), 400);
	}
	if (!$seo_url_en = $request->get('seo_url_en')) {
		return $app->json(array('message' => "Field seo_url_en is required for country", 'status' => 'failure'), 400);
	}
	$country->setLabelFr($label_fr);
	$country->setLabelEn($label_en);
	$country->setSeoUrlFr($seo_url_fr);
	$country->setSeoUrlEn($seo_url_en);
	$entityManager->persist($country);
	$entityManager->flush();
	return $app->json(array('status' => 'success', 'result' => "Country " . $country->getIdCountry() . " created"), 201);

});

$app->put('/country/{id}', function (Request $request, $id) use ($entityManager, $app) {
	// update country

	$country = $entityManager->find('country', $id);

	if (!$country) {
		return $app->json(array('message' => "Country not found", 'status' => 'failure'), 404);
	}
	if (!$label_fr = $request->get('label_fr')) {
		return $app->json(array('message' => "Field label_fr is required for country", 'status' => 'failure'), 400);
	}
	if (!$label_en = $request->get('label_en')) {
		return $app->json(array('message' => "Field label_en is required for country", 'status' => 'failure'), 400);
	}
	if (!$seo_url_fr = $request->get('seo_url_fr')) {
		return $app->json(array('message' => "Field seo_url_fr is required for country", 'status' => 'failure'), 400);
	}
	if (!$seo_url_en = $request->get('seo_url_en')) {
		return $app->json(array('message' => "Field seo_url_en is required for country", 'status' => 'failure'), 400);
	}
	$country->setLabelFr($label_fr);
	$country->setLabelEn($label_en);
	$country->setSeoUrlFr($seo_url_fr);
	$country->setSeoUrlEn($seo_url_en);
	$entityManager->persist($country);
	$entityManager->flush();
	return $app->json(array('status' => 'success', 'result' => "Country $id updated"), 200);
});

$app->delete("/country/{id}", function($id) use($entityManager, $app) {
	$country = $entityManager->find('country', $id);

	if (!$country) {
		return $app->json(array('message' => "Country not found", 'status' => 'failure'), 404);
	}

	$entityManager->remove($country);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Country $id deleted"), 200);
});

$app->get('/artist', function() use ($app) {
	// get all artists

	$sql = "SELECT * FROM artist";
	$artists = $app['db']->fetchAll($sql);
		
	foreach ($artists as $key => $artist) {
		$sql = "SELECT * FROM country WHERE id_country = ?";
		$country = $app['db']->fetchAssoc($sql, array((int) $artists[$key]['id_Country']));
		unset($artists[$key]['id_Country']);
		$artists[$key]['country'] = $country;
	}

	return $app->json(array('status' => 'success', 'result' => $artists, 'X-Total-Count' => count($artists)), 200);	

});

$app->get('/artist/{id}', function ($id) use($app) {
	// get 1 artist
	$sql = "SELECT * FROM artist WHERE id_artist = ?";
	$post = $app['db']->fetchAssoc($sql, array((int) $id));
		
	if (!$post) {
		return $app->json(array('message' => 'Artist not found', 'status' => 'failure'), 404);
	}
	$sql = "SELECT * FROM country WHERE id_country = ?";
	$country = $app['db']->fetchAssoc($sql, array((int) $post['id_Country']));
	unset($post['id_Country']);
	$post['country'] = $country;

	return $app->json(array('status' => 'success', 'result' => $post, 'X-Total-Count' => 1), 200);

});

$app->post('/artist', function (Request $request) use($entityManager, $app) {
	// create new artist
	
	if (!$firstname = $request->get('firstname')) {
		return $app->json(array('message' => "Field firstname is required for artist", 'status' => 'failure'), 400);
	}
	if (!$lastname = $request->get('lastname')) {
		return $app->json(array('message' => "Field lastname is required for artist", 'status' => 'failure'), 400);
	}
	if (!$birthday = $request->get('birthday')) {
		return $app->json(array('message' => "Field birthday is required for artist", 'status' => 'failure'), 400);
	}
	if (!$biography = $request->get('biography')) {
		return $app->json(array('message' => "Field biography is required for artist", 'status' => 'failure'), 400);
	}
	if (!$id_country = $request->get('id_country')) {
		return $app->json(array('message' => "Field id_country is required for artist", 'status' => 'failure'), 400);
	}
	$country = $entityManager->find('country', $id_country);
	if (!$country) {
		return $app->json(array('message' => "Country matching id $id_country not found", 'status' => 'failure'), 400);
	}
	$artist = new Artist();
	$artist->setFirstName($firstname);
	$artist->setLastName($lastname);
	$artist->setBirthday(new DateTime($birthday));
	$artist->setBiography($biography);
	$artist->setCountry($country);
	$entityManager->persist($artist);
	$entityManager->flush();
	return $app->json(array('status' => 'success', 'result' => "Artist " . $artist->getIdArtist() . " created"), 201);
	
});

$app->put('/artist/{id}', function (Request $request, $id) use ($entityManager, $app) {
	// update artist

	$artist = $entityManager->find('artist', $id);

	if (!$artist) {
		return $app->json(array('message' => "Artist not found", 'status' => 'failure'), 404);
	}
	if (!$firstname = $request->get('firstname')) {
		return $app->json(array('message' => "Field firstname is required for artist", 'status' => 'failure'), 400);
	}
	if (!$lastname = $request->get('lastname')) {
		return $app->json(array('message' => "Field lastname is required for artist", 'status' => 'failure'), 400);
	}
	if (!$birthday = $request->get('birthday')) {
		return $app->json(array('message' => "Field birthday is required for artist", 'status' => 'failure'), 400);
	}
	if (!$biography = $request->get('biography')) {
		return $app->json(array('message' => "Field biography is required for artist", 'status' => 'failure'), 400);
	}
	if (!$id_country = $request->get('id_country')) {
		return $app->json(array('message' => "Field id_country is required for artist", 'status' => 'failure'), 400);
	}
	$country = $entityManager->find('country', $id_country);
	if (!$country) {
		return $app->json(array('message' => "Country matching id $id_country not found", 'status' => 'failure'), 400);
	}
	$artist->setFirstName($firstname);
	$artist->setLastName($lastname);
	$artist->setBirthday(new DateTime($birthday));
	$artist->setBiography($biography);
	$artist->setCountry($country);
	$entityManager->persist($artist);
	$entityManager->flush();
	return $app->json(array('status' => 'success', 'result' => "Artist $id updated"), 200);
});

$app->delete("/artist/{id}", function($id) use($entityManager, $app) {
	$artist = $entityManager->find('artist', $id);

	if (!$artist) {
		return $app->json(array('message' => "Artist not found", 'status' => 'failure'), 404);
	}

	$entityManager->remove($artist);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Artist $id deleted"), 200);
});

$app->get('/artwork', function() use ($app) {
	// get all artwork

	$sql = "SELECT * FROM artwork";
	$artworks = $app['db']->fetchAll($sql);
		
	foreach ($artworks as $key => $artwork) {
		$sql = "SELECT * FROM artist WHERE id_artist = ?";
		$artist = $app['db']->fetchAssoc($sql, array((int) $artworks[$key]['id_artist']));
		unset($artworks[$key]['id_artist']);
		$artworks[$key]['artist'] = $artist;

		$sql = "SELECT * FROM category WHERE id_category = ?";
		$category = $app['db']->fetchAssoc($sql, array((int) $artworks[$key]['id_category']));
		unset($artworks[$key]['id_category']);
		$artworks[$key]['category'] = $category;

		$sql = "SELECT * FROM gallery WHERE id_gallery = ?";
		$gallery = $app['db']->fetchAssoc($sql, array((int) $artworks[$key]['id_gallery']));
		unset($artworks[$key]['id_gallery']);
		$artworks[$key]['gallery'] = $gallery;

		$dim = new dimensions();
		foreach (unserialize($artworks[$key]['artwork_dimensions']) as $dimKey => $value) $dim->{$dimKey} = $value;
		$artworks[$key]['artwork_dimensions'] = $dim;
	}

	return $app->json(array('status' => 'success', 'result' => $artworks, 'X-Total-Count' => count($artworks)), 200);	

});

$app->get('/artwork/{id}', function ($id) use($app) {
	// get 1 artwork
	$sql = "SELECT * FROM artwork WHERE id_artwork = ?";
	$post = $app['db']->fetchAssoc($sql, array((int) $id));
		
	if (!$post) {
		return new Response("Artwork not found", 404);
	}


	$sql = "SELECT * FROM artist WHERE id_artist = ?";
	$artist = $app['db']->fetchAssoc($sql, array((int) $post['id_artist']));
	unset($post['id_artist']);
	$post['artist'] = $artist;

	$sql = "SELECT * FROM category WHERE id_category = ?";
	$category = $app['db']->fetchAssoc($sql, array((int) $post['id_category']));
	unset($post['id_category']);
	$post['category'] = $category;

	$sql = "SELECT * FROM gallery WHERE id_gallery = ?";
	$gallery = $app['db']->fetchAssoc($sql, array((int) $post['id_gallery']));
	unset($post['id_gallery']);
	$post['gallery'] = $gallery;

	$dim = new dimensions();
	foreach (unserialize($post['artwork_dimensions']) as $key => $value) $dim->{$key} = $value;
	$post['artwork_dimensions'] = $dim;

	return $app->json(array('status' => 'success', 'result' => $post, 'X-Total-Count' => 1), 200);

});

$app->post('/artwork', function (Request $request) use($entityManager, $app) {
	// create new artwork
	
	if (!$title = $request->get('title')) {
		return $app->json(array('message' => "Field title is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$biography = $request->get('biography')) {
		return $app->json(array('message' => "Field biography is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$year = $request->get('year')) {
		return $app->json(array('message' => "Field year is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$price = $request->get('price')) {
		return $app->json(array('message' => "Field price is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$dimensions = $request->get('dimensions')) {
		return $app->json(array('message' => "Field dimensions is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$is_certificated = $request->get('is_certificated')) {
		return $app->json(array('message' => "Field is_certificated is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$is_framed = $request->get('is_framed')) {
		return $app->json(array('message' => "Field is_framed is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$is_numbered = $request->get('is_numbered')) {
		return $app->json(array('message' => "Field is_numbered is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$id_artist = $request->get('id_artist')) {
		return $app->json(array('message' => "Field id_artist is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$id_category = $request->get('id_category')) {
		return $app->json(array('message' => "Field id_category is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$id_gallery = $request->get('id_gallery')) {
		return $app->json(array('message' => "Field id_gallery is required for artwork", 'status' => 'failure'), 400);
	}

	$artist = $entityManager->find('artist', $id_artist);
	if (!$artist) {
		return $app->json(array('message' => "Artist matching id $id_artist not found", 'status' => 'failure'), 400);
	}
	$category = $entityManager->find('category', $id_category);
	if (!$artist) {
		return $app->json(array('message' => "Category matching id $id_category not found", 'status' => 'failure'), 400);
	}
	$gallery = $entityManager->find('gallery', $id_gallery);
	if (!$gallery) {
		return $app->json(array('message' => "Gallery matching id $id_gallery not found", 'status' => 'failure'), 400);
	}

	$artwork = new Artwork();
	$artwork->setArtist($artist);
	$artwork->setCategory($category);
	$artwork->setGallery($gallery);

	$artwork->setTitle($title);
	$artwork->setBiography($biography);
	$artwork->setArtworkYear($year);
	$artwork->setArtworkPrice($price);

	$dim = new dimensions();
	foreach ($dimensions as $key => $value) $dim->{$key} = $value;
	$artwork->setArtworkDimensions($dim);

	$artwork->setIsCertificated(filter_var($is_certificated, FILTER_VALIDATE_BOOLEAN));
	$artwork->setIsFramed(filter_var($is_framed, FILTER_VALIDATE_BOOLEAN));
	$artwork->setIsNumbered(filter_var($is_numbered, FILTER_VALIDATE_BOOLEAN));

	$entityManager->persist($artwork);
	$entityManager->flush();
	return $app->json(array('status' => 'success', 'result' => "Artwork " . $artwork->getIdArtwork() . " created"), 201);
	
});

$app->put('/artwork/{id}', function (Request $request, $id) use ($entityManager, $app) {
	// update artwork

	$artwork = $entityManager->find('artwork', $id);

	if (!$artwork) {
		return $app->json(array('message' => "Artwork not found", 'status' => 'failure'), 404);
	}
		if (!$title = $request->get('title')) {
		return $app->json(array('message' => "Field title is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$biography = $request->get('biography')) {
		return $app->json(array('message' => "Field biography is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$year = $request->get('year')) {
		return $app->json(array('message' => "Field year is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$price = $request->get('price')) {
		return $app->json(array('message' => "Field price is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$dimensions = $request->get('dimensions')) {
		return $app->json(array('message' => "Field dimensions is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$is_certificated = $request->get('is_certificated')) {
		return $app->json(array('message' => "Field is_certificated is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$is_framed = $request->get('is_framed')) {
		return $app->json(array('message' => "Field is_framed is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$is_numbered = $request->get('is_numbered')) {
		return $app->json(array('message' => "Field is_numbered is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$id_artist = $request->get('id_artist')) {
		return $app->json(array('message' => "Field id_artist is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$id_category = $request->get('id_category')) {
		return $app->json(array('message' => "Field id_category is required for artwork", 'status' => 'failure'), 400);
	}
	if (!$id_gallery = $request->get('id_gallery')) {
		return $app->json(array('message' => "Field id_gallery is required for artwork", 'status' => 'failure'), 400);
	}

	$artist = $entityManager->find('artist', $id_artist);
	if (!$artist) {
		return $app->json(array('message' => "Artist matching id $id_artist not found", 'status' => 'failure'), 400);
	}
	$category = $entityManager->find('category', $id_category);
	if (!$artist) {
		return $app->json(array('message' => "Category matching id $id_category not found", 'status' => 'failure'), 400);
	}
	$gallery = $entityManager->find('gallery', $id_gallery);
	if (!$gallery) {
		return $app->json(array('message' => "Gallery matching id $id_gallery not found", 'status' => 'failure'), 400);
	}
	$artwork->setArtist($artist);
	$artwork->setCategory($category);
	$artwork->setGallery($gallery);

	$artwork->setTitle($title);
	$artwork->setBiography($biography);
	$artwork->setArtworkYear($year);
	$artwork->setArtworkPrice($price);

	$dim = new dimensions();
	foreach ($dimensions as $key => $value) $dim->{$key} = $value;
	$artwork->setArtworkDimensions($dim);

	$artwork->setIsCertificated(filter_var($is_certificated, FILTER_VALIDATE_BOOLEAN));
	$artwork->setIsFramed(filter_var($is_framed, FILTER_VALIDATE_BOOLEAN));
	$artwork->setIsNumbered(filter_var($is_numbered, FILTER_VALIDATE_BOOLEAN));
	$entityManager->persist($artwork);
	$entityManager->flush();
	return $app->json(array('status' => 'success', 'result' => "Artwork $id updated"), 200);
});

$app->delete("/artwork/{id}", function($id) use($entityManager, $app) {
	$artwork = $entityManager->find('artwork', $id);

	if (!$artwork) {
		return $app->json(array('message' => "Artwork not found", 'status' => 'failure'), 404);
	}

	$entityManager->remove($artwork);
	$entityManager->flush();

	return $app->json(array('status' => 'success', 'result' => "Artwork $id deleted"), 200);
});



$app->run();
?>