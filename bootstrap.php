<?php
require_once __DIR__ . "/vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(__DIR__ . "/class");
$isDevMode = true;

// the connection configuration
$dbParams = array(
	'driver'   => 'pdo_mysql',
	'user'     => 'root',
	'password' => '',
	'dbname'   => 'rest',
	);


$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, "data/DoctrineORMModule/Proxy", null, false);
$entityManager = EntityManager::create($dbParams, $config);
