<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_category", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id_category;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_name", type="string", length=255, nullable=false)
     */
    private $seoName;

	/**
	* @ORM\OneToMany(targetEntity="artwork", mappedBy="category")
	* @var artworks[]
	**/
	protected $artworks = null;

	public function getIdCategory(){
		return $this->id_category;
	}

	public function getLabel(){
		return $this->label;
	}

	public function setLabel($label){
		$this->label = $label;
	}

	public function getSeoName(){
		return $this->seoName;
	}

	public function setSeoName($seoName){
		$this->seoName = $seoName;
	}

	public function addArtwork($artwork) {
		$this->artworks[] = $artwork;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->artworks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Remove artworks
     *
     * @param \artwork $artworks
     */
    public function removeArtwork(\artwork $artworks)
    {
        $this->artworks->removeElement($artworks);
    }

    /**
     * Get artworks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArtworks()
    {
        return $this->artworks;
    }
}
