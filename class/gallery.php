<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Gallery
 *
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Gallery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_gallery", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idGallery;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

	/**
	* @ORM\OneToMany(targetEntity="artwork", mappedBy="gallery")
	* @var artworks[]
	**/
	protected $artworks = null;

	public function getIdGallery(){
		return $this->idGallery;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function addArtwork($artwork) {
		$this->artworks[] = $artwork;
	}

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->artworks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Remove artworks
     *
     * @param \artwork $artworks
     */
    public function removeArtwork(\artwork $artworks)
    {
        $this->artworks->removeElement($artworks);
    }

    /**
     * Get artworks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArtworks()
    {
        return $this->artworks;
    }
}
