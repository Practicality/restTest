<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Artist
 *
 * @ORM\Table(name="artist")
 * @ORM\Entity
 */
class Artist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_artist", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idArtist;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="biography", type="text", length=65535, nullable=true)
     */
    private $biography;

	/**
	 * @ORM\ManyToOne(targetEntity="country", inversedBy="artists")
	 * @ORM\JoinColumn(name="id_country", referencedColumnName="id_country")
	 **/
    private $country;

	/**
	* @ORM\OneToMany(targetEntity="artwork", mappedBy="artist")
	* @var artworks[]
	**/
	protected $artworks = null;

	public function getIdArtist(){
		return $this->idArtist;
	}

	public function getFirstname(){
		return $this->firstname;
	}

	public function setFirstname($firstname){
		$this->firstname = $firstname;
	}

	public function getLastname(){
		return $this->lastname;
	}

	public function setLastname($lastname){
		$this->lastname = $lastname;
	}

	public function getBirthday(){
		return $this->birthday;
	}

	public function setBirthday(DateTime $birthday){
		$this->birthday = $birthday;
	}

	public function getBiography(){
		return $this->biography;
	}

	public function setBiography($biography){
		$this->biography = $biography;
	}

	public function getCountry(){
		return $this->country;
	}

	public function setCountry($country){
		$country->addArtist($this);
		$this->country = $country;
	}

	public function addArtwork($artwork) {
		$this->artworks[] = $artwork;
	}


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->artworks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Remove artworks
     *
     * @param \artwork $artworks
     */
    public function removeArtwork(\artwork $artworks)
    {
        $this->artworks->removeElement($artworks);
    }

    /**
     * Get artworks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArtworks()
    {
        return $this->artworks;
    }
}
