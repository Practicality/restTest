<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_country", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="label_fr", type="string", length=255, nullable=true)
     */
    private $labelFr;

    /**
     * @var string
     *
     * @ORM\Column(name="label_en", type="string", length=255, nullable=true)
     */
    private $labelEn;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_url_fr", type="string", length=255, nullable=true)
     */
    private $seoUrlFr;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_url_en", type="string", length=255, nullable=true)
     */
    private $seoUrlEn;

	/**
	* @ORM\OneToMany(targetEntity="Artist", mappedBy="country")
	* @var artists[]
	**/
	protected $artists = null;

	public function getIdCountry(){
		return $this->idCountry;
	}

	public function getLabelFr(){
		return $this->labelFr;
	}

	public function setLabelFr($labelFr){
		$this->labelFr = $labelFr;
	}

	public function getLabelEn(){
		return $this->labelEn;
	}

	public function setLabelEn($labelEn){
		$this->labelEn = $labelEn;
	}

	public function getSeoUrlFr(){
		return $this->seoUrlFr;
	}

	public function setSeoUrlFr($seoUrlFr){
		$this->seoUrlFr = $seoUrlFr;
	}

	public function getSeoUrlEn(){
		return $this->seoUrlEn;
	}

	public function setSeoUrlEn($seoUrlEn){
		$this->seoUrlEn = $seoUrlEn;
	}

	public function addArtist($artist)
	{
		$this->artists[] = $artist;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->artists = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Remove artists
     *
     * @param \Artist $artists
     */
    public function removeArtist(\Artist $artists)
    {
        $this->artists->removeElement($artists);
    }

    /**
     * Get artists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArtists()
    {
        return $this->artists;
    }
}
