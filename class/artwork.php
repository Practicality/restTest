<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Artwork
 *
 * @ORM\Table(name="artwork")
 * @ORM\Entity
 */
class Artwork
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_artwork", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idArtwork;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="artwork_dimensions", type="object", nullable=false)
     */
    private $artworkDimensions;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="text", length=255, nullable=true)
	 */
	private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="biography", type="text", length=65535, nullable=true)
     */
    private $biography;

    /**
     * @var integer
     *
     * @ORM\Column(name="artwork_year", type="integer", nullable=true)
     */
    private $artworkYear;

    /**
     * @var string
     *
     * @ORM\Column(name="artwork_price", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $artworkPrice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_certificated", type="boolean", nullable=true)
     */
    private $isCertificated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_framed", type="boolean", nullable=true)
     */
    private $isFramed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_numbered", type="boolean", nullable=true)
     */
    private $isNumbered;
    private $galleryId;

	/**
	* @ORM\ManyToOne(targetEntity="category", inversedBy="artworks")
	* @ORM\JoinColumn(name="id_category", referencedColumnName="id_category")
	**/
	private $category;

	/**
	* @ORM\ManyToOne(targetEntity="gallery", inversedBy="artworks")
	* @ORM\JoinColumn(name="id_gallery", referencedColumnName="id_gallery")
	**/
	private $gallery;

	/**
	* @ORM\ManyToOne(targetEntity="artist", inversedBy="artworks")
	* @ORM\JoinColumn(name="id_artist", referencedColumnName="id_artist")
	**/
	private $artist;

	public function getIdArtwork(){
		return $this->idArtwork;
	}

	public function getArtworkDimensions(){
		return $this->artworkDimensions;
	}

	public function setArtworkDimensions($artworkDimensions){
		$this->artworkDimensions = $artworkDimensions;
	}

	public function getBiography(){
		return $this->biography;
	}

	public function setBiography($biography){
		$this->biography = $biography;
	}

	public function getArtworkYear(){
		return $this->artworkYear;
	}

	public function setArtworkYear($artworkYear){
		$this->artworkYear = $artworkYear;
	}

	public function getArtworkPrice(){
		return $this->artworkPrice;
	}

	public function setArtworkPrice($artworkPrice){
		$this->artworkPrice = $artworkPrice;
	}

	public function getIsCertificated(){
		return $this->isCertificated;
	}

	public function setIsCertificated($isCertificated){
		$this->isCertificated = $isCertificated;
	}

	public function getIsFramed(){
		return $this->isFramed;
	}

	public function setIsFramed($isFramed){
		$this->isFramed = $isFramed;
	}

	public function getIsNumbered(){
		return $this->isNumbered;
	}

	public function setIsNumbered($isNumbered){
		$this->isNumbered = $isNumbered;
	}

	public function setCategory($category){
		$category->addArtwork($this);
		$this->category = $category;
	}

	public function getCategory() {
		return $this->category;
	}

	public function setGallery($gallery){
		$gallery->addArtwork($this);
		$this->gallery = $gallery;
	}

	public function getGallery() {
		return $this->gallery;
	}

	public function setArtist($artist){
		$artist->addArtwork($this);
		$this->artist = $artist;
	}

	public function getArtist() {
		return $this->artist;
	}


    /**
     * Set title
     *
     * @param string $title
     * @return Artwork
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
}
